FROM ubuntu:latest
ENV http_proxy http://10.114.23.201:80
ENV https_proxy http://10.114.23.201:80
ENV no_proxy registry.gear.ge.com
RUN apt-get update && apt-get install -y python3 python3-pip libcairo2-dev pango1.0-tests

RUN python3 -m pip install --upgrade setuptools ez_setup && python3 -m pip install jinja2 pandas WeasyPrint

ADD test.py /files/
ADD template.html /files/
ADD summary.html /files/
ADD assets /files/assets/

WORKDIR /files

ENTRYPOINT ["python3", "test.py"]

