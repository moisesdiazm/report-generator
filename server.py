from flask import Flask, render_template

app = Flask(__name__, template_folder='new_report')


@app.route('/')
def helloWorld():
    dataFrame = {
        "mntSerialNumber": "TS7600-4833B6",
        "cstSite": "Evendale Test Cell 5",
        "startDate": "2015-09-03",
        "shudtdowns": "0(0.00%)",
        "endDate": "2016-05-02",
        "engineCycles": 181,
        "hoursMonitored": 8928.78,
        "hoursOperated": "138.83 (1.55%)",
        "hoursHighLoad": "66.07 (47.5%)",
        "gapHours": 16,
        "aggresiveness": 1.366,
        "nggPeakList": (" ").join(["9975.01", "(2015-12-18)", "9975.01", "(2015-12-18)","9975.01", "(2015-12-18)","9975.01", "(2015-12-18)","9975.01", "(2015-12-18)","9975.01", "(2015-12-18)","9975.01", "(2015-12-18)","9975.01", "(2015-12-18)"]),
        "nptPeakList": (" ").join(["9975.01", "(2015-12-18)","9975.01", "(2015-12-18)","9975.01", "(2015-12-18)","9975.01", "(2015-12-18)","9975.01", "(2015-12-18)"]),
        "partialEngineCycles": "55",
        "fullEngineCycles": "117 (64.64%)"


    }
    return render_template('template.html', **dataFrame)


if __name__ == "__main__":
    app.run(port=8081, debug=True)