from __future__ import print_function
import pandas as pd
import numpy as np
import argparse
from jinja2 import Environment, FileSystemLoader
from weasyprint import HTML

if __name__=="__main__":
    parser = argparse.ArgumentParser("PDF Report generator")
    parser.add_argument('infile', type=argparse.FileType('r'),help="Report source file in excel")
    parser.add_argument('outfile', type=argparse.FileType('w'), help="PDF out file")
    args = parser.parse_args()
    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template("template.html")
    template_vars = {"title": "GEIQ Report", "managers": [
        {
            "name": "moises",
            "CPU": 21
        },
        {
            "name": "Juan",
            "CPU": 32
        }
    ]}

    html_out = template.render(template_vars)
    HTML(string=html_out).write_pdf("/src/" + args.outfile.name, stylesheets=["./assets/plugins/font-awesome/css/font-awesome.css", "./assets/plugins/prism/prism.css", "./assets/plugins/elegant_font/css/style.css", "./assets/css/styles.css"])